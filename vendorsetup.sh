FDEVICE="CTL_sprout"

fox_get_target_device() {
local chkdev=$(echo "$BASH_SOURCE" | grep $FDEVICE)
   if [ -n "$chkdev" ]; then
      FOX_BUILD_DEVICE="$FDEVICE"
   else
      chkdev=$(set | grep BASH_ARGV | grep $FDEVICE)
      [ -n "$chkdev" ] && FOX_BUILD_DEVICE="$FDEVICE"
   fi
}

if [ -z "$1" -a -z "$FOX_BUILD_DEVICE" ]; then
   fox_get_target_device
fi

if [ "$1" = "$FDEVICE" -o "$FOX_BUILD_DEVICE" = "$FDEVICE" ]; then
    export OF_AB_DEVICE=1

    export FOX_USE_BASH_SHELL=1
    export FOX_ASH_IS_BASH=1
    export FOX_USE_NANO_EDITOR=1
    export FOX_USE_TAR_BINARY=1
    export FOX_USE_ZIP_BINARY=1
    export FOX_REPLACE_BUSYBOX_PS=1
    export OF_USE_NEW_MAGISKBOOT=1
    export OF_SKIP_MULTIUSER_FOLDERS_BACKUP=1

    export OF_USE_SYSTEM_FINGERPRINT=1

    # Additional
    export FOX_RECOVERY_INSTALL_PARTITION="/dev/block/bootdevice/by-name/boot"
    #export FOX_RECOVERY_SYSTEM_PARTITION="/dev/block/bootdevice/by-name/system"
    #export FOX_RECOVERY_VENDOR_PARTITION="/dev/block/bootdevice/by-name/vendor"

    # Developer
    export OF_MAINTAINER="KonstantIMP"

    # Display
    export OF_SCREEN_H=2280

    # R11
    export FOX_R11=1
    export FOX_ADVANCED_SECURITY=1
    export OF_USE_TWRP_SAR_DETECT=1
    export OF_DISABLE_MIUI_OTA_BY_DEFAULT=1
    export OF_QUICK_BACKUP_LIST="/boot;/data;/system_image;/vendor_image;"

    # Building
    export ALLOW_MISSING_DEPENDENCIES=true
    export FOX_USE_TWRP_RECOVERY_IMAGE_BUILDER=1
    export LC_ALL="C"

    # Kernel
    export FOX_BUILD_FULL_KERNEL_SOURCES=0

    export ARCH="arm64"
    export SUBARCH="arm64"
   
    export TARGET_DEVICE="CTL_sprout"
    export OF_TARGET_DEVICES="CTL_sprout,CTL,Crystal,crystal"
    export TARGET_DEVICE_ALT="CTL_sprout, CTL, Crystal, crystal"

	# let's see what are our build VARs
	if [ -n "$FOX_BUILD_LOG_FILE" -a -f "$FOX_BUILD_LOG_FILE" ]; then
  	   export | grep "FOX" >> $FOX_BUILD_LOG_FILE
  	   export | grep "OF_" >> $FOX_BUILD_LOG_FILE
   	   export | grep "TARGET_" >> $FOX_BUILD_LOG_FILE
  	   export | grep "TW_" >> $FOX_BUILD_LOG_FILE
 	fi

	add_lunch_combo omni_"$FDEVICE"-eng
	add_lunch_combo omni_"$FDEVICE"-userdebug
fi
#



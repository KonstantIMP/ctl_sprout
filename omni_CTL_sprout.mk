# Release name
PRODUCT_RELEASE_NAME := CTL_sprout

# Add APNs
$(call inherit-product, vendor/omni/config/gsm.mk)

# Stock vendor config
$(call inherit-product, vendor/omni/config/common.mk)

# Add languages support
$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)

# Add telephony sopport
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# WIPE list
TARGET_RECOVERY_WIPE := \
    device/nokia/CTL_sprout/recovery/root/etc/recovery.wipe	

# OTA partitions list
AB_OTA_PARTITIONS += \
boot \
system \
vendor

# PostOTAinstall cfg
AB_OTA_POSTINSTALL_CONFIG += \
RUN_POSTINSTALL_system=true \
POSTINSTALL_PATH_system=system/bin/otapreopt_script \
FILESYSTEM_TYPE_system=ext4 \
POSTINSTALL_OPTIONAL_system=true

# Updaters
PRODUCT_PACKAGES += \
otapreopt_script \
update_engine \
update_verifier

# Boot control HAL
PRODUCT_PACKAGES += \
bootctrl.sdm660

# FULL OTA and AB support
PRODUCT_STATIC_BOOT_CONTROL_HAL := \
bootctrl.sdm660 \
libgptutils \
libz

# Charger
PRODUCT_PACKAGES += \
charger_res_images \
charger

# Device IDs
PRODUCT_NAME := omni_CTL_sprout
PRODUCT_DEVICE := CTL_sprout
PRODUCT_BRAND := Nokia
PRODUCT_MODEL := Nokia 7.1
PRODUCT_MANUFACTURER := Nokia

# Enable zip packages flash
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    ro.secure=1 \
    ro.adb.secure=0 \
    ro.allow.mock.location=0 \
    ro.vendor.build.security_patch=2099-12-31


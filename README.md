# OrangeFox device tree for Nokia 7.1

## Device specifications

| Device   | Nokia 7.1                            |
| -------: | :----------------------------------- |
| Codename | CTL sprout                           |
| CPU      | Qualcomm Snapdragon 636              |
| GPU      | Adreno 509                           |
| RAM      | 3/4 Gb                               |
| ROM      | 32/64 Gb                             |
| Battery  | Non-removable, 3060 mAh Li-ion       |
| Display  | 2280*1080 FHD, 432 dpi, HDR+ support |
